package de.tiiunder.flatmate.service;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class EventService {
    private Bus bus = new Bus(ThreadEnforcer.ANY);
    private static EventService instance;

    public static EventService getInstance() {
        if(instance == null) {
            instance = new EventService();
        }

        return instance;
    }

    public void post(Object event) {
        bus.post(event);
    }

    public void register(Object object) {
        bus.register(object);
    }

    public void unregister(Object object) {
        bus.unregister(object);
    }
}
