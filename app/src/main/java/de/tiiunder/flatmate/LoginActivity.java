package de.tiiunder.flatmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.events.user.LoginUserEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.user.LoginUserTask;
import de.tiiunder.flatmate.service.EventService;

public class LoginActivity extends ActionBarActivity {
    private EventService eventService;

    public static final String PREFERENCES_FILE = "preferences-user-file";
    public static final String PREFERENCES_USER_USERNAME = "preferences-user-username";
    public static final String PREFERENCES_USER_PASSWORD = "preferecnes-user-password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnRegister = (Button) findViewById(R.id.btnRegister);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                new LoginUserTask(username, password).execute();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServerConstants.SERVER_WEB_URL + "en/register")); // TODO use correct locale
                Intent chooserIntent = Intent.createChooser(intent, getResources().getString(R.string.dialog_select_browser));
                startActivity(chooserIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventService = EventService.getInstance();
        eventService.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        eventService.unregister(this);
    }

    @Subscribe
    public void storeUserCredentials(LoginUserEvent event) {
        Log.d("Event", "Login User Event fetched");

        if(event.isSuccessful()) {
            SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(PREFERENCES_USER_USERNAME, event.getUsername());
            editor.putString(PREFERENCES_USER_PASSWORD, event.getPassword());
            editor.commit();

            setResult(Activity.RESULT_OK);
            finish();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
