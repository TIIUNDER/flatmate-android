package de.tiiunder.flatmate.rest.category;

import android.content.Context;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.category.DeleteCategoryEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class DeleteCategoryTask extends SuperAsyncTask {
    private Category category;

    public DeleteCategoryTask(Context context, Category category) {
        super(context);

        this.category = category;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "category/" + category.getId() + "/delete";

            ResponseEntity<Object> responseEntity = restTemplate.exchange(restUrl, HttpMethod.DELETE, new HttpEntity<Object>(requestHeaders), Object.class);

            HttpStatus status = responseEntity.getStatusCode(); // send request

            return status.value();

        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == org.apache.http.HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Delete Category Event pushed");

            eventService.post(new DeleteCategoryEvent());
        }
    }
}
