package de.tiiunder.flatmate.rest.expense;

import android.content.Context;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.entities.expense.SingleExpense;
import de.tiiunder.flatmate.events.expense.PostExpenseEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class PostExpenseTask extends SuperAsyncTask {
    private Expense expense;

    public PostExpenseTask(Context context, Expense expense) {
        super(context);

        this.expense = expense;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "expense/post";

            SingleExpense singleExpense = new SingleExpense();
            singleExpense.setExpense(expense);

            HttpEntity<SingleExpense> requestEntity = new HttpEntity<>(singleExpense, requestHeaders);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(restUrl, HttpMethod.POST, requestEntity, Object.class);

            HttpStatus status = responseEntity.getStatusCode(); // send request

            return status.value();
        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == org.apache.http.HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Post Expense Event pushed");

            eventService.post(new PostExpenseEvent());
        }
    }
}
