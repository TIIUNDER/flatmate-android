package de.tiiunder.flatmate.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpStatus;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import de.tiiunder.flatmate.LoginActivity;
import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.service.EventService;

public class SuperAsyncTask extends AsyncTask {
    protected RestTemplate restTemplate;
    protected HttpHeaders requestHeaders;
    private Context context;

    public SuperAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        requestHeaders = new HttpHeaders();

        SharedPreferences sharedPreferences = context.getSharedPreferences(LoginActivity.PREFERENCES_FILE, Context.MODE_PRIVATE);
        String username = sharedPreferences.getString(LoginActivity.PREFERENCES_USER_USERNAME, null);
        String password = sharedPreferences.getString(LoginActivity.PREFERENCES_USER_PASSWORD, null);

        Log.d("Shared Preferences", "Username: " + username);
        Log.d("Shared Preferences", "Password: " + password);

        HttpBasicAuthentication authentication = new HttpBasicAuthentication(username, password);

        requestHeaders.setAuthorization(authentication);

        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        // TODO handle more codes
        String message = null;

        try {
            if ((int) result >= HttpStatus.SC_BAD_REQUEST) { // client and server errors (>400)

                switch ((int) result) {
                    case HttpStatus.SC_UNAUTHORIZED:
                        message = context.getResources().getString(R.string.error_status_unauthorized);
                        break;
                    case HttpStatus.SC_FORBIDDEN:
                        message = context.getResources().getString(R.string.error_status_forbidden);
                        break;
                    case HttpStatus.SC_NOT_FOUND:
                        message = context.getResources().getString(R.string.error_status_not_found);
                        break;
                    default:
                        message = context.getResources().getString(R.string.error_status_default);
                        break;
                }

                EventService eventService = EventService.getInstance();
                eventService.post(new ErrorEvent());

            }
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }

        if(message != null) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
