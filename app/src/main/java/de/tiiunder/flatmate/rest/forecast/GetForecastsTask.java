package de.tiiunder.flatmate.rest.forecast;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.forecast.Forecasts;
import de.tiiunder.flatmate.events.forecast.GetForecastsEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class GetForecastsTask extends SuperAsyncTask {
    private Forecasts forecasts;

    public GetForecastsTask(Context context) {
        super(context);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "forecast/";

            ResponseEntity<Forecasts> responseEntity = restTemplate.exchange(restUrl, HttpMethod.GET, new HttpEntity<Object>(requestHeaders), Forecasts.class);

            forecasts = responseEntity.getBody();

            return responseEntity.getStatusCode().value();
        } catch(HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Get Forecasts Event pushed");

            eventService.post(new GetForecastsEvent(forecasts));
        }
    }
}
