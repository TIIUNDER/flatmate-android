package de.tiiunder.flatmate.rest.consumption;

import android.content.Context;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.entities.consumption.SingleConsumption;
import de.tiiunder.flatmate.events.consumption.PostConsumptionEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class PostConsumptionTask extends SuperAsyncTask {
    private Consumption consumption;

    public PostConsumptionTask(Context context, Consumption consumption) {
        super(context);

        this.consumption = consumption;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "consumption/post";

            SingleConsumption singleConsumption = new SingleConsumption();
            singleConsumption.setConsumption(consumption);

            HttpEntity<SingleConsumption> requestEntity = new HttpEntity<>(singleConsumption, requestHeaders);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(restUrl, HttpMethod.POST, requestEntity, Object.class);

            HttpStatus status = responseEntity.getStatusCode(); // send request

            return status.value();
        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == org.apache.http.HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Post Consumption Event pushed");

            eventService.post(new PostConsumptionEvent());
        }
    }
}
