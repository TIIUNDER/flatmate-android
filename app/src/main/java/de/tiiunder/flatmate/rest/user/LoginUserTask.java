package de.tiiunder.flatmate.rest.user;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import de.tiiunder.flatmate.entities.user.SingleUser;
import de.tiiunder.flatmate.entities.user.User;
import de.tiiunder.flatmate.events.user.LoginUserEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.service.EventService;

public class LoginUserTask extends AsyncTask {
    private String username;
    private String password;

    public LoginUserTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            HttpHeaders requestHeaders = new HttpHeaders();

            String restUrl = ServerConstants.SERVER_API_URL + "user/login";

            User user = new User(username, password);
            SingleUser singleUser = new SingleUser();
            singleUser.setUser(user);

            HttpEntity<SingleUser> requestEntity = new HttpEntity<>(singleUser, requestHeaders);

            ResponseEntity<Boolean> responseEntity = restTemplate.exchange(restUrl, HttpMethod.POST, requestEntity, Boolean.class);

            boolean loginSuccessful = responseEntity.getBody();

            Log.d("Login", Boolean.toString(loginSuccessful));

            return loginSuccessful;
        } catch (Exception ex) {
            ex.printStackTrace();

            return false;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        EventService eventService = EventService.getInstance();

        Log.d("Event", "Login User Event pushed");

        eventService.post(new LoginUserEvent((Boolean) result, username, password));
    }
}
