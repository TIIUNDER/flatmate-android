package de.tiiunder.flatmate.rest.expense;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpStatus;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.expense.Expenses;
import de.tiiunder.flatmate.events.expense.GetExpensesEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class GetExpensesTask extends SuperAsyncTask {
    private Expenses expenses;

    public GetExpensesTask(Context context) {
        super(context);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "expense/";

            ResponseEntity<Expenses> responseEntity = restTemplate.exchange(restUrl, HttpMethod.GET, new HttpEntity<Object>(requestHeaders), Expenses.class);

            expenses = responseEntity.getBody();

            return responseEntity.getStatusCode().value();
        } catch(HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Get Expenses Event pushed");

            eventService.post(new GetExpensesEvent(expenses));
        }
    }
}
