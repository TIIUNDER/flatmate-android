package de.tiiunder.flatmate.rest.category;

import android.content.Context;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.category.SingleCategory;
import de.tiiunder.flatmate.events.category.UpdateCategoryEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class UpdateCategoryTask extends SuperAsyncTask {
    private Category category;

    public UpdateCategoryTask(Context context, Category category) {
        super(context);

        this.category = category;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "category/" + category.getId() + "/update";
            category.setId(0);

            SingleCategory singleCategory = new SingleCategory();
            singleCategory.setCategory(category);

            HttpEntity<SingleCategory> requestEntity = new HttpEntity<>(singleCategory, requestHeaders);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(restUrl, HttpMethod.POST, requestEntity, Object.class);

            HttpStatus status = responseEntity.getStatusCode(); // send request

            return status.value();

        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == org.apache.http.HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Update Category Event pushed");

            eventService.post(new UpdateCategoryEvent());
        }
    }
}
