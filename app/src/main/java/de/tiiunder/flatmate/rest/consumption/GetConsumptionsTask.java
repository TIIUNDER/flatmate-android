package de.tiiunder.flatmate.rest.consumption;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import de.tiiunder.flatmate.entities.consumption.Consumptions;
import de.tiiunder.flatmate.events.consumption.GetConsumptionsEvent;
import de.tiiunder.flatmate.rest.ServerConstants;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class GetConsumptionsTask extends SuperAsyncTask {
    private Consumptions consumptions;

    public GetConsumptionsTask(Context context) {
        super(context);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            super.doInBackground(params);

            String restUrl = ServerConstants.SERVER_API_URL + "consumption/";

            ResponseEntity<Consumptions> responseEntity = restTemplate.exchange(restUrl, HttpMethod.GET, new HttpEntity<Object>(requestHeaders), Consumptions.class);

            consumptions = responseEntity.getBody();

            return responseEntity.getStatusCode().value();
        } catch(HttpClientErrorException ex) {
            ex.printStackTrace();

            return ex.getStatusCode().value();
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if((int) result == HttpStatus.SC_OK) {
            EventService eventService = EventService.getInstance();

            Log.d("Event", "Get Consumptions Event pushed");

            eventService.post(new GetConsumptionsEvent(consumptions));
        }
    }
}
