package de.tiiunder.flatmate.events.user;

public class LoginUserEvent {
    private boolean successful;
    private String username;
    private String password;

    public LoginUserEvent(boolean successful, String username, String password) {
        this.successful = successful;

        this.username = username;
        this.password = password;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
