package de.tiiunder.flatmate.events.category;

import de.tiiunder.flatmate.entities.category.Categories;

public class GetCategoriesEvent {
    private Categories categories;

    public GetCategoriesEvent(Categories categories) {
        this.categories = categories;
    }

    public Categories getCategories() {
        return categories;
    }

}
