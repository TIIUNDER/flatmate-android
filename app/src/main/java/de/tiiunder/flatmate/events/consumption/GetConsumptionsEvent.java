package de.tiiunder.flatmate.events.consumption;

import de.tiiunder.flatmate.entities.consumption.Consumptions;

public class GetConsumptionsEvent {
    private Consumptions consumptions;

    public GetConsumptionsEvent(Consumptions consumptions) {
        this.consumptions = consumptions;
    }

    public Consumptions getConsumptions() {
        return consumptions;
    }
}
