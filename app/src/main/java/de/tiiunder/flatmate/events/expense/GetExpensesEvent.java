package de.tiiunder.flatmate.events.expense;

import de.tiiunder.flatmate.entities.expense.Expenses;

public class GetExpensesEvent {
    private Expenses expenses;

    public GetExpensesEvent(Expenses expenses) {
        this.expenses = expenses;
    }

    public Expenses getExpenses() {
        return expenses;
    }

}
