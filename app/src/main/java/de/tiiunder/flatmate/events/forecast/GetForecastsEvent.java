package de.tiiunder.flatmate.events.forecast;

import de.tiiunder.flatmate.entities.forecast.Forecasts;

public class GetForecastsEvent {
    private Forecasts forecasts;

    public GetForecastsEvent(Forecasts forecasts) {
        this.forecasts = forecasts;
    }

    public Forecasts getForecasts() {
        return forecasts;
    }
}
