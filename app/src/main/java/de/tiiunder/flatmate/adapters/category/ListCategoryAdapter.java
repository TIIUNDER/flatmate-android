package de.tiiunder.flatmate.adapters.category;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import de.tiiunder.flatmate.adapters.SuperListAdapter;
import de.tiiunder.flatmate.entities.category.Category;

import java.util.ArrayList;

public class ListCategoryAdapter<Object> extends SuperListAdapter<Object> {

    public ListCategoryAdapter(Context context, int resource, ArrayList<Object> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = super.getView(position, row, parent);

        Category category = (Category) objects.get(position);

        String primary = category.getName();
        String secondary = category.getUnit();

        tvPrimary.setText(primary);
        tvSecondary.setText(secondary);

        return row;
    }
}
