package de.tiiunder.flatmate.adapters.expense;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.SuperListAdapter;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.types.DateType;
import de.tiiunder.flatmate.types.PeriodType;

public class ListExpenseAdapter<Object> extends SuperListAdapter<Object> {

    public ListExpenseAdapter(Context context, int resource, ArrayList<Object> objects, boolean loadCategories) {
        super(context, resource, objects, loadCategories);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = super.getView(position, row, parent);

        Expense expense = (Expense) objects.get(position);

        DateType date = expense.getDate();
        String expenseDate =
                date.getYear() + "-" +
                (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) + "-" +
                (date.getDay() < 10 ? "0" + date.getDay() : date.getDay());
        String expenseName = expense.getName();
        String expensePeriod = context.getResources().getString(R.string.string_every) + " " + expense.getPeriodCount() + " " + PeriodType.periodName(context, expense.getPeriodType());
        String expenseCount = new DecimalFormat("#.#").format(expense.getCount());
        String expenseConsumption = new DecimalFormat("#.#").format(expense.getConsumption());
        String expenseDeposit = new DecimalFormat("#.##").format(expense.getDeposit());
        String expenseFee = new DecimalFormat("#.##").format(expense.getFee()) + "€ " + context.getResources().getString(R.string.string_every) + " " + expense.getFeePeriodCount() + " " + PeriodType.periodName(context, expense.getFeePeriodType());
        String expenseCost = new DecimalFormat("#.##").format(expense.getCostPerUnit()) + "€ " + context.getResources().getString(R.string.string_per_unit);

        String expenseCategory = "";
        String expenseUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == expense.getCategoryId()) {
                    expenseCategory = category.getName();
                    expenseUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        String primary = expenseName;
        String secondary = expenseCategory + " | " + expenseDate + " | " + expensePeriod + " | " + expenseCount + expenseUnit + " | " + expenseConsumption + expenseUnit +
                " | " + expenseDeposit + " | " + expenseFee + " | " + expenseCost;

        tvPrimary.setText(primary);
        tvSecondary.setText(secondary);

        return row;
    }

    @Subscribe
    public void fillCategories(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        categories = event.getCategories();
        this.notifyDataSetChanged();
    }
}
