package de.tiiunder.flatmate.adapters.forecast;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.SuperListAdapter;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.forecast.Forecast;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;

public class ListForecastAdapter<Object> extends SuperListAdapter<Object> {

    public ListForecastAdapter(Context context, int resource, ArrayList arrayList, boolean loadCategories) {
        super(context, resource, arrayList, loadCategories);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = super.getView(position, row, parent);

        Forecast forecast = (Forecast) objects.get(position);

        String forecastCategory = "";
        String forecastUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == forecast.getCategoryId()) {
                    forecastCategory = category.getName();
                    forecastUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        String name = forecast.getName();
        String startDate = new DateTime(forecast.getStartDate()).toString("yyyy-MM-dd");
        String endDate = new DateTime(forecast.getEndDate()).toString("yyyy-MM-dd");
        String expectedConsumption = new DecimalFormat("#.#").format(forecast.getExpectedConsumption()) + forecastUnit;
        String consumptionUntilNow = new DecimalFormat("#.#").format(forecast.getConsumptionUntilNow()) + forecastUnit;
        String expectedCosts = new DecimalFormat("#.##").format(forecast.getExpectedCosts()) + "€";
        String costsUntilNow = new DecimalFormat("#.##").format(forecast.getCostsUntilNow()) + "€";

        String primary = name;
        String secondary = forecastCategory + " | " + expectedConsumption + " " + context.getResources().getString(R.string.string_until) + " "
                + endDate + " | " + consumptionUntilNow + " " + context.getResources().getString(R.string.string_since) + " " + startDate
                + " | " + expectedCosts + " " + context.getResources().getString(R.string.string_until) + " " + endDate
                + " | " + costsUntilNow + " " + context.getResources().getString(R.string.string_since) + " " + startDate;

        tvPrimary.setText(primary);
        tvSecondary.setText(secondary);

        return row;
    }

    @Subscribe
    public void fillCategories(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        categories = event.getCategories();
        this.notifyDataSetChanged();
    }
}
