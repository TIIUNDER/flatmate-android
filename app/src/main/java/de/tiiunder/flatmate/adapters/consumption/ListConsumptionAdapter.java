package de.tiiunder.flatmate.adapters.consumption;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import de.tiiunder.flatmate.adapters.SuperListAdapter;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;

public class ListConsumptionAdapter<Object> extends SuperListAdapter<Object> {

    public ListConsumptionAdapter(Context context, int resource, ArrayList<Object> objects, boolean loadCategories) {
        super(context, resource, objects, loadCategories);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = super.getView(position, row, parent);

        Consumption consumption = (Consumption) objects.get(position);

        String consumptionValue = new DecimalFormat("#.#").format(consumption.getValue());
        Date date = consumption.getCreatedAt();
        DateTime dateTime = new DateTime(date);
        String consumptionDate = dateTime.toString("yyyy-MM-dd HH:mm");

        String consumptionCategory = "";
        String consumptionUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == consumption.getCategoryId()) {
                    consumptionCategory = category.getName();
                    consumptionUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        String primary = consumption.getName();
        String secondary = consumptionCategory + " | " + consumptionValue + consumptionUnit + " | " + consumptionDate;

        tvPrimary.setText(primary);
        tvSecondary.setText(secondary);

        return row;
    }

    @Subscribe
    public void fillCategories(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        categories = event.getCategories();
        this.notifyDataSetChanged();
    }
}
