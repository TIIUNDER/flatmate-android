package de.tiiunder.flatmate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Categories;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;
import de.tiiunder.flatmate.service.EventService;

public class SuperListAdapter<Object> extends ArrayAdapter<Object> {
    protected Context context;
    protected int resource;
    protected ArrayList<Object> objects;
    protected Categories categories;

    protected TextView tvPrimary;
    protected TextView tvSecondary;

    private EventService eventService;

    public SuperListAdapter(Context context, int resource, ArrayList<Object> objects) {
        this(context, resource, objects, false);
    }

    public SuperListAdapter(Context context, int resource, ArrayList<Object> objects, boolean loadCategories) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.objects = objects;

        eventService = EventService.getInstance();
        eventService.register(this);

        if(loadCategories) {
            new GetCategoriesTask(context).execute();
        }
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = LayoutInflater.from(context).inflate(resource, parent, false);

        if (position % 2 != 0) {
            row.setBackgroundColor(context.getResources().getColor(R.color.background_item_color_odd));
        }

        tvPrimary = (TextView) row.findViewById(R.id.tvPrimary);
        tvSecondary = (TextView) row.findViewById(R.id.tvSecondary);

        return row;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();

        eventService.unregister(this);
    }
}
