package de.tiiunder.flatmate;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import de.tiiunder.flatmate.fragments.category.ListCategoryFragment;
import de.tiiunder.flatmate.fragments.consumption.ListConsumptionFragment;
import de.tiiunder.flatmate.fragments.expense.ListExpenseFragment;
import de.tiiunder.flatmate.fragments.NavigationDrawerFragment;
import de.tiiunder.flatmate.fragments.forecast.ListForecastFragment;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private FragmentManager mFragmentManager;
    private boolean lastScreenReached;

    public static final int LOGIN_ACTIVITY_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentManager = getSupportFragmentManager();
        lastScreenReached = false;

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                mFragmentManager.findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position, boolean addToBackstack) {

        switch (position+1) {
            case NavigationDrawerFragment.NAV_ITEM_EXPENSES:
                mTitle = getString(R.string.title_expenses);
                changeFragment(new ListExpenseFragment(), addToBackstack);
                break;
            case NavigationDrawerFragment.NAV_ITEM_CONSUMPTIONS:
                mTitle = getString(R.string.title_consumptions);
                changeFragment(new ListConsumptionFragment(), addToBackstack);
                break;
            case NavigationDrawerFragment.NAV_ITEM_CATEGORIES:
                mTitle = getString(R.string.title_categories);
                changeFragment(new ListCategoryFragment(), addToBackstack);
                break;
            case NavigationDrawerFragment.NAV_ITEM_FORECAST:
                mTitle = getString(R.string.title_forecasts);
                changeFragment(new ListForecastFragment(), addToBackstack);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            mFragmentManager.popBackStack();
        } else {
            if (!lastScreenReached) {
                lastScreenReached = true;
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.notice_close_app), Toast.LENGTH_LONG).show();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void setTitle(String title) {
        mTitle = title;
        restoreActionBar();
    }

    public void changeFragment(Fragment fragment, boolean addToBackstack) {
        if(mFragmentManager != null && fragment != null) {
            FragmentTransaction fragmentTransaction = mFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, fragment);

            if (addToBackstack) {
                fragmentTransaction.addToBackStack(null);
            }

            fragmentTransaction.commit();
        }
    }

}
