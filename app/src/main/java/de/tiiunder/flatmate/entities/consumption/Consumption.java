package de.tiiunder.flatmate.entities.consumption;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.util.Date;

@JsonRootName("consumption")
public class Consumption implements Serializable {

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int id;

    @JsonProperty("category_id")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int categoryId; // used for GET

    @JsonProperty
    private int category; // used for POST

    @JsonProperty
    private String name;

    @JsonProperty
    private double value;

    @JsonProperty("created_at")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Date createdAt;

    @JsonProperty("user_id")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Consumption{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", createdAt=" + createdAt +
                ", userId=" + userId +
                '}';
    }
}
