package de.tiiunder.flatmate.entities.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;

@JsonRootName("user")
public class User implements Serializable {

    @JsonProperty
    private String username;

    @JsonProperty
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
