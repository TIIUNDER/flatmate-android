package de.tiiunder.flatmate.entities.category;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SingleCategory implements Serializable {

    @JsonProperty
    private Category category;

    public void setCategory(Category category) {
        this.category = category;
    }
}
