package de.tiiunder.flatmate.entities.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SingleUser implements Serializable {

    @JsonProperty
    private User user; // wraps user as object into JSON request entity

    public void setUser(User user) {
        this.user = user;
    }
}
