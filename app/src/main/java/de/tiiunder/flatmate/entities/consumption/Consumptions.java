package de.tiiunder.flatmate.entities.consumption;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.util.ArrayList;

@JsonRootName("consumptions")
public class Consumptions implements Serializable {

    private ArrayList<Consumption> consumptions = new ArrayList<>();

    public ArrayList<Consumption> getConsumptions() {
        return consumptions;
    }

    public void setConsumptions(ArrayList<Consumption> consumptions) {
        this.consumptions = consumptions;
    }
}
