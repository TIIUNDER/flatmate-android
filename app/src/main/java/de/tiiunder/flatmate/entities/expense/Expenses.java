package de.tiiunder.flatmate.entities.expense;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;
import java.util.ArrayList;

@JsonRootName("expenses")
public class Expenses implements Serializable {

    private ArrayList<Expense> expenses = new ArrayList<>();

    public ArrayList<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }
}
