package de.tiiunder.flatmate.entities.consumption;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleConsumption {

    @JsonProperty
    private Consumption consumption;

    public void setConsumption(Consumption consumption) {
        this.consumption = consumption;
    }
}
