package de.tiiunder.flatmate.entities.forecast;

import java.io.Serializable;
import java.util.Date;

public class Forecast implements Serializable {

    private int id;
    private Date startDate;
    private Date endDate;
    private double consumptionUntilNow;
    private double expectedConsumption;
    private double costsUntilNow;
    private double expectedCosts;
    private String name;
    private int categoryId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getConsumptionUntilNow() {
        return consumptionUntilNow;
    }

    public void setConsumptionUntilNow(double consumptionUntilNow) {
        this.consumptionUntilNow = consumptionUntilNow;
    }

    public double getExpectedConsumption() {
        return expectedConsumption;
    }

    public void setExpectedConsumption(double expectedConsumption) {
        this.expectedConsumption = expectedConsumption;
    }

    public double getCostsUntilNow() {
        return costsUntilNow;
    }

    public void setCostsUntilNow(double costsUntilNow) {
        this.costsUntilNow = costsUntilNow;
    }

    public double getExpectedCosts() {
        return expectedCosts;
    }

    public void setExpectedCosts(double expectedCosts) {
        this.expectedCosts = expectedCosts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", consumptionUntilNow=" + consumptionUntilNow +
                ", expectedConsumption=" + expectedConsumption +
                ", costsUntilNow=" + costsUntilNow +
                ", expectedCosts=" + expectedCosts +
                ", name='" + name + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
