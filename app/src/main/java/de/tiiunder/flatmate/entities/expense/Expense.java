package de.tiiunder.flatmate.entities.expense;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import de.tiiunder.flatmate.types.DateType;

@JsonRootName("expense")
public class Expense implements Serializable {

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int id;

    @JsonProperty("user_id")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int userId;

    @JsonProperty("category_id")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int categoryId; // used for GET

    @JsonProperty
    private int category; // used for POST

    @JsonProperty
    private DateType date;

    @JsonProperty
    private String name;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private double count;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private double consumption;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private double deposit;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private double fee;

    @JsonProperty("fee_period_count")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int feePeriodCount;

    @JsonProperty("fee_period_type")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int feePeriodType;

    @JsonProperty("cost_per_unit")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private double costPerUnit;

    @JsonProperty("period_count")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int periodCount;

    @JsonProperty("period_type")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int periodType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public DateType getDate() {
        return date;
    }

    public void setDate(DateType date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getFeePeriodCount() {
        return feePeriodCount;
    }

    public void setFeePeriodCount(int feePeriodCount) {
        this.feePeriodCount = feePeriodCount;
    }

    public int getFeePeriodType() {
        return feePeriodType;
    }

    public void setFeePeriodType(int feePeriodType) {
        this.feePeriodType = feePeriodType;
    }

    public double getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(double costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public int getPeriodCount() {
        return periodCount;
    }

    public void setPeriodCount(int periodCount) {
        this.periodCount = periodCount;
    }

    public int getPeriodType() {
        return periodType;
    }

    public void setPeriodType(int periodType) {
        this.periodType = periodType;
    }

    @Override
    public String toString() {
        return "Expense{" +
                "id=" + id +
                ", userId=" + userId +
                ", categoryId=" + categoryId +
                ", date=" + date +
                ", name='" + name + '\'' +
                ", count=" + count +
                ", consumption=" + consumption +
                ", deposit=" + deposit +
                ", fee=" + fee +
                ", feePeriodCount=" + feePeriodCount +
                ", feePeriodType=" + feePeriodType +
                ", costPerUnit=" + costPerUnit +
                ", periodCount=" + periodCount +
                ", periodType=" + periodType +
                '}';
    }
}
