package de.tiiunder.flatmate.entities.forecast;


import java.io.Serializable;
import java.util.ArrayList;

public class Forecasts implements Serializable {

    private ArrayList<Forecast> forecasts = new ArrayList<>();

    public ArrayList<Forecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(ArrayList<Forecast> forecasts) {
        this.forecasts = forecasts;
    }
}
