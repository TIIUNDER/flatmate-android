package de.tiiunder.flatmate.entities.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;

@JsonRootName("category")
public class Category implements Serializable {

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int id;

    @JsonProperty("user_id")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int userId;

    @JsonProperty
    private String name;

    @JsonProperty
    private String unit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        String output;
        if(unit == null) {
            output = name;
        } else {
            output = name + " (" + unit + ")";
        }
        return output;
    }
}
