package de.tiiunder.flatmate.entities.expense;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleExpense {

    @JsonProperty
    private Expense expense;

    public void setExpense(Expense expense) {
        this.expense = expense;
    }
}
