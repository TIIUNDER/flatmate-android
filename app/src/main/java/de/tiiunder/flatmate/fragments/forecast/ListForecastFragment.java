package de.tiiunder.flatmate.fragments.forecast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.forecast.ListForecastAdapter;
import de.tiiunder.flatmate.entities.forecast.Forecast;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.forecast.GetForecastsEvent;
import de.tiiunder.flatmate.fragments.ListFragment;
import de.tiiunder.flatmate.rest.forecast.GetForecastsTask;

public class ListForecastFragment extends ListFragment {
    private boolean forecastsLoaded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Forecasts list fragment started");

        btnAdd.setVisibility(View.GONE);

        new GetForecastsTask(getActivity().getApplicationContext()).execute();

        return view;
    }

    @Subscribe
    public void fillForecastsList(GetForecastsEvent event) {
        Log.d("Event", "Get Forecasts Event fetched");

        // hide loading animation if forecasts AND categories are ready
        forecastsLoaded = true;
        if(categoriesLoaded) {
            progressBar.setVisibility(View.GONE);
        }

        ArrayList<Forecast> forecasts = event.getForecasts().getForecasts();

        if(forecasts.isEmpty()) {
            tvEmpty.setText(getResources().getString(R.string.message_list_empty_forecast));
            tvEmpty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        } else {
            ListForecastAdapter<Forecast> adapter = new ListForecastAdapter(getActivity(), R.layout.list_item, forecasts, true);

            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Forecast forecast = (Forecast) listView.getItemAtPosition(position);

                    SingleForecastFragment fragment = new SingleForecastFragment();
                    fragment.setForecast(forecast);

                    redirectFragment(fragment);
                }
            });
        }
    }

    @Subscribe
    public void categoriesLoaded(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        // hide loading animation if forecasts AND categories are ready
        categoriesLoaded = true;
        if(forecastsLoaded) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        tvEmpty.setText(getResources().getString(R.string.message_list_empty_forecast));
        super.handleError(event);
    }
}
