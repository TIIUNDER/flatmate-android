package de.tiiunder.flatmate.fragments.category;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.category.PostCategoryEvent;
import de.tiiunder.flatmate.rest.category.PostCategoryTask;

public class AddCategoryFragment extends FormCategoryFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = super.onCreateView(inflater, container, savedInstanceState);

        Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Category category = handleFormData();

                    new PostCategoryTask(getActivity().getApplicationContext(), category).execute();
                }
            }
        });

        return view;
    }

    @Subscribe
    public void redirectToList(PostCategoryEvent event) {
        Log.d("Event", "Post Category Event fetched");

        redirectFragment(new ListCategoryFragment());
    }
}
