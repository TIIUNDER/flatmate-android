package de.tiiunder.flatmate.fragments.expense;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.expense.ListExpenseAdapter;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.expense.GetExpensesEvent;
import de.tiiunder.flatmate.fragments.ListFragment;
import de.tiiunder.flatmate.rest.expense.GetExpensesTask;

public class ListExpenseFragment extends ListFragment {
    private boolean expensesLoaded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Expenses list fragment started");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectFragment(new AddExpenseFragment());
            }
        });

        new GetExpensesTask(getActivity().getApplicationContext()).execute();

        return view;
    }

    @Subscribe
    public void fillExpensesList(GetExpensesEvent event) {
        Log.d("Event", "Get Expenses Event fetched");

        // hide loading animation if expenses AND categories are ready
        expensesLoaded = true;
        if(categoriesLoaded) {
            progressBar.setVisibility(View.GONE);
        }

        ArrayList<Expense> expenses = event.getExpenses().getExpenses();

        if(expenses.isEmpty()) {
            tvEmpty.setText(getResources().getString(R.string.message_list_empty_expense));
            tvEmpty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        } else {
            ListExpenseAdapter<Expense> adapter = new ListExpenseAdapter<>(getActivity(), R.layout.list_item, expenses, true);

            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Expense expense = (Expense) listView.getItemAtPosition(position);

                    SingleExpenseFragment fragment = new SingleExpenseFragment();
                    fragment.setExpense(expense);

                    redirectFragment(fragment);
                }
            });
        }
    }

    @Subscribe
    public void categoriesLoaded(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        // hide loading animation if expenses AND categories are ready
        categoriesLoaded = true;
        if(expensesLoaded) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        tvEmpty.setText(getResources().getString(R.string.message_list_empty_expense));
        super.handleError(event);
    }
}
