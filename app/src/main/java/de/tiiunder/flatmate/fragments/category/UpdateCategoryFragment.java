package de.tiiunder.flatmate.fragments.category;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.category.UpdateCategoryEvent;
import de.tiiunder.flatmate.rest.category.UpdateCategoryTask;

public class UpdateCategoryFragment extends FormCategoryFragment {
    private Category category;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if(category != null) {
            etName.setText(category.getName().toString());
            etUnit.setText(category.getUnit().toString());
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Category handleCategory = handleFormData();
                    handleCategory.setId(category.getId());

                    new UpdateCategoryTask(getActivity().getApplicationContext(), handleCategory).execute();
                }
            }
        });

        return view;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Subscribe
    public void redirectToList(UpdateCategoryEvent event) {
        Log.d("Event", "Update Category Event fetched");

        redirectFragment(new ListCategoryFragment());
    }
}
