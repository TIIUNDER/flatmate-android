package de.tiiunder.flatmate.fragments.category;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.DeleteCategoryEvent;
import de.tiiunder.flatmate.fragments.SingleFragment;
import de.tiiunder.flatmate.rest.category.DeleteCategoryTask;

public class SingleCategoryFragment extends SingleFragment {
    private Category category;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Category single fragment started");

        if(category != null) {
            tvHeader.setText(getActivity().getString(R.string.title_category));

            showCategoryFields();
        }

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateCategoryFragment fragment = new UpdateCategoryFragment();
                fragment.setCategory(category);

                redirectFragment(fragment);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.newInstance(
                        new DeleteCategoryTask(getActivity().getApplicationContext(), category),
                        null,
                        getResources().getString(R.string.dialog_confirm_delete_category)
                );
                confirmDialog.show(getFragmentManager(), getString(R.string.dialog_confirm));
            }
        });

        return view;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Subscribe
    public void redirectToList(DeleteCategoryEvent event) {
        Log.d("Event", "Delete Category Event fetched");

        redirectFragment(new ListCategoryFragment());
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        super.handleError(event);
    }

    public void showCategoryFields() {
        TextView tvName = new TextView(activity);
        TextView tvUnit = new TextView(activity);

        tvName.setText(activity.getResources().getString(R.string.form_label_name) + ": " + category.getName());
        tvUnit.setText(activity.getResources().getString(R.string.form_label_unit) + ": " + category.getUnit());

        tvName.setLayoutParams(layoutParams);
        tvUnit.setLayoutParams(layoutParams);

        layoutContent.addView(tvName);
        layoutContent.addView(tvUnit);

        progressBar.setVisibility(View.GONE);
        tvHeader.setVisibility(View.VISIBLE);
        layoutContent.setVisibility(View.VISIBLE);
    }
}
