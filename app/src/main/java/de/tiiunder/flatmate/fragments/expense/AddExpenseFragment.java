package de.tiiunder.flatmate.fragments.expense;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.events.expense.PostExpenseEvent;
import de.tiiunder.flatmate.rest.expense.PostExpenseTask;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;

public class AddExpenseFragment extends FormExpenseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Expense handleExpense = handleFormData();

                    new PostExpenseTask(getActivity().getApplicationContext(), handleExpense).execute();
                }
            }
        });

        return view;
    }

    @Subscribe
    public void fillCategoriesSpinner(GetCategoriesEvent event) {
        super.fillCategoriesSpinner(event);
    }

    @Subscribe
    public void redirectToList(PostExpenseEvent event) {
        Log.d("Event", "Post Expense Event fetched");

        redirectFragment(new ListExpenseFragment());
    }
}
