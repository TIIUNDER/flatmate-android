package de.tiiunder.flatmate.fragments.expense;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.joda.time.DateTime;

import java.text.DecimalFormat;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Categories;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.expense.DeleteExpenseEvent;
import de.tiiunder.flatmate.fragments.SingleFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;
import de.tiiunder.flatmate.rest.expense.DeleteExpenseTask;
import de.tiiunder.flatmate.types.DateType;
import de.tiiunder.flatmate.types.PeriodType;

public class SingleExpenseFragment extends SingleFragment {
    private Expense expense;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Expense single fragment started");

        if(expense != null) {
            tvHeader.setText(getActivity().getString(R.string.title_expense));

            new GetCategoriesTask(activity.getApplicationContext()).execute();
        }

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateExpenseFragment fragment = new UpdateExpenseFragment();
                fragment.setExpense(expense);

                redirectFragment(fragment);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.newInstance(
                        new DeleteExpenseTask(getActivity().getApplicationContext(), expense),
                        null,
                        getResources().getString(R.string.dialog_confirm_delete_expense)
                );
                confirmDialog.show(getFragmentManager(), getString(R.string.dialog_confirm));
            }
        });

        return view;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    @Subscribe
    public void redirectToList(DeleteExpenseEvent event) {
        Log.d("Event", "Delete Expense Event fetched");

        redirectFragment(new ListExpenseFragment());
    }

    @Subscribe
    public void showExpenseFieldsAndCategory(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        Categories categories = event.getCategories();

        TextView tvCategory = new TextView(activity);
        TextView tvDate = new TextView(activity);
        TextView tvName = new TextView(activity);
        TextView tvPeriod = new TextView(activity);
        TextView tvCount = new TextView(activity);
        TextView tvConsumption = new TextView(activity);
        TextView tvDeposit = new TextView(activity);
        TextView tvFee = new TextView(activity);
        TextView tvCostPerUnit = new TextView(activity);

        String categoryName = "";
        String categoryUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == expense.getCategoryId()) {
                    categoryName = category.getName();
                    categoryUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        DateType date = expense.getDate();
        String expenseDate =
                date.getYear() + "-" +
                (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) + "-" +
                (date.getDay() < 10 ? "0" + date.getDay() : date.getDay());

        tvCategory.setText(activity.getResources().getString(R.string.form_label_category) + ": " + categoryName);
        tvDate.setText(activity.getResources().getString(R.string.form_label_date) + ": " + expenseDate);
        tvName.setText(activity.getResources().getString(R.string.form_label_name) + ": " + expense.getName());
        tvPeriod.setText(activity.getResources().getString(R.string.form_label_period_count) + ": " + expense.getPeriodCount() + " " + PeriodType.periodName(activity.getApplicationContext(), expense.getPeriodType()));
        tvCount.setText(activity.getResources().getString(R.string.form_label_count) + ": " + new DecimalFormat("#.#").format(expense.getCount()) + categoryUnit);
        tvConsumption.setText(activity.getResources().getString(R.string.form_label_consumption) + ": " + new DecimalFormat("#.#").format(expense.getConsumption()) + categoryUnit);
        tvDeposit.setText(activity.getResources().getString(R.string.form_label_deposit) + ": " + new DecimalFormat("#.##").format(expense.getDeposit()) + "€");
        tvFee.setText(activity.getResources().getString(R.string.form_label_fee) + ": " + new DecimalFormat("#.##").format(expense.getFee()) + "€ " + activity.getResources().getString(R.string.string_every) + " " + expense.getFeePeriodCount() + " " + PeriodType.periodName(activity.getApplicationContext(), expense.getFeePeriodType()));
        tvCostPerUnit.setText(activity.getResources().getString(R.string.form_label_cost_per_unit) + ": " + new DecimalFormat("#.##").format(expense.getCostPerUnit()) + "€");

        tvCategory.setLayoutParams(layoutParams);
        tvDate.setLayoutParams(layoutParams);
        tvName.setLayoutParams(layoutParams);
        tvPeriod.setLayoutParams(layoutParams);
        tvCount.setLayoutParams(layoutParams);
        tvConsumption.setLayoutParams(layoutParams);
        tvDeposit.setLayoutParams(layoutParams);
        tvFee.setLayoutParams(layoutParams);
        tvCostPerUnit.setLayoutParams(layoutParams);

        layoutContent.addView(tvCategory);
        layoutContent.addView(tvDate);
        layoutContent.addView(tvName);
        layoutContent.addView(tvPeriod);
        layoutContent.addView(tvCount);
        layoutContent.addView(tvConsumption);
        layoutContent.addView(tvDeposit);
        layoutContent.addView(tvFee);
        layoutContent.addView(tvCostPerUnit);

        progressBar.setVisibility(View.GONE);
        tvHeader.setVisibility(View.VISIBLE);
        layoutContent.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        super.handleError(event);
    }
}
