package de.tiiunder.flatmate.fragments.consumption;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.consumption.PostConsumptionEvent;
import de.tiiunder.flatmate.rest.consumption.PostConsumptionTask;

public class AddConsumptionFragment extends FormConsumptionFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Consumption handleConsumption = handleFormData();

                    new PostConsumptionTask(getActivity().getApplicationContext(), handleConsumption).execute();
                }
            }
        });

        return view;
    }

    @Subscribe
    public void fillCategoriesSpinner(GetCategoriesEvent event) {
        super.fillCategoriesSpinner(event);
    }

    @Subscribe
    public void redirectToList(PostConsumptionEvent event) {
        Log.d("Event", "Post Consumption Event fetched");

        redirectFragment(new ListConsumptionFragment());
    }
}
