package de.tiiunder.flatmate.fragments.consumption;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.consumption.UpdateConsumptionEvent;
import de.tiiunder.flatmate.rest.consumption.UpdateConsumptionTask;

public class UpdateConsumptionFragment extends FormConsumptionFragment {
    private Consumption consumption;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if(consumption != null) {
            etName.setText(consumption.getName().toString());
            etValue.setText(String.valueOf(consumption.getValue()));
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Consumption handleConsumption = handleFormData();
                    handleConsumption.setId(consumption.getId());

                    new UpdateConsumptionTask(getActivity().getApplicationContext(), handleConsumption).execute();
                }
            }
        });

        return view;
    }

    public void setConsumption(Consumption consumption) {
        this.consumption = consumption;
    }

    @Subscribe
    public void fillCategoriesSpinner(GetCategoriesEvent event) {
        super.fillCategoriesSpinner(event);

        if(consumption != null) {
            int categoryCount = 0;
            // search and select correct category
            for (Category category : event.getCategories().getCategories()) {
                if (category.getId() == consumption.getCategoryId()) {
                    spinnerCategory.setSelection(categoryCount);
                    break;
                }
                categoryCount++;
            }
        }
    }

    @Subscribe
    public void redirectToList(UpdateConsumptionEvent event) {
        Log.d("Event", "Update Consumption Event fetched");

        redirectFragment(new ListConsumptionFragment());
    }
}
