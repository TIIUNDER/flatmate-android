package de.tiiunder.flatmate.fragments.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.category.ListCategoryAdapter;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.fragments.ListFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;

public class ListCategoryFragment extends ListFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Categories list fragment started");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectFragment(new AddCategoryFragment());
            }
        });

        new GetCategoriesTask(getActivity().getApplicationContext()).execute();

        return view;
    }

    @Subscribe
    public void fillCategoriesList(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event catched");

        progressBar.setVisibility(View.GONE);

        ArrayList<Category> categories = event.getCategories().getCategories();

        if(categories.isEmpty()) {
            tvEmpty.setText(getResources().getString(R.string.message_list_empty_category));
            tvEmpty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        } else {
            ListCategoryAdapter<Category> adapter = new ListCategoryAdapter<>(getActivity(), R.layout.list_item, categories);

            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Category category = (Category) listView.getItemAtPosition(position);

                    SingleCategoryFragment fragment = new SingleCategoryFragment();
                    fragment.setCategory(category);

                    redirectFragment(fragment);
                }
            });
        }
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        tvEmpty.setText(getResources().getString(R.string.message_list_empty_category));
        super.handleError(event);
    }
}
