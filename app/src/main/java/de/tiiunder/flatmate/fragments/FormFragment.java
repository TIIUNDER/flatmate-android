package de.tiiunder.flatmate.fragments;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Categories;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.fragments.category.AddCategoryFragment;
import de.tiiunder.flatmate.rest.expense.DeleteExpenseTask;

public class FormFragment extends SuperFragment {
    protected View view;
    protected Spinner spinnerCategory;

    protected void fillCategoriesSpinner(GetCategoriesEvent event) {
        ArrayList<Category> categories = event.getCategories().getCategories();

        if (categories.isEmpty()) {
            // dialog for adding a new category
            ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.newInstance(
                    null,
                    new AddCategoryFragment(),
                    getResources().getString(R.string.dialog_new_category)
            );
            confirmDialog.show(getFragmentManager(), getString(R.string.dialog_confirm));
        } else {
            // fill category spinner with categories
            ArrayAdapter<Category> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            if (spinnerCategory != null) {
                spinnerCategory.setAdapter(adapter);
            }
        }

        // add empty category item for selecting "none"
        Category emptyCategorySelect = new Category();
        emptyCategorySelect.setId(0);
        emptyCategorySelect.setName(getResources().getString(R.string.string_none));
        categories.add(0, emptyCategorySelect);
    }

    protected void setupDatePicker(final EditText etDate) {
        etDate.setFocusable(false);

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etDate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });
    }

}
