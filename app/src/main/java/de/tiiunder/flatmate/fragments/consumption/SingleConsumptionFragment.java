package de.tiiunder.flatmate.fragments.consumption;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.joda.time.DateTime;

import java.text.DecimalFormat;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Categories;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.consumption.DeleteConsumptionEvent;
import de.tiiunder.flatmate.fragments.SingleFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;
import de.tiiunder.flatmate.rest.consumption.DeleteConsumptionTask;

public class SingleConsumptionFragment extends SingleFragment {
    private Consumption consumption;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Consumption single fragment started");

        if(consumption != null) {
            tvHeader.setText(getActivity().getString(R.string.title_consumption));

            new GetCategoriesTask(activity.getApplicationContext()).execute();
        }

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateConsumptionFragment fragment = new UpdateConsumptionFragment();
                fragment.setConsumption(consumption);

                redirectFragment(fragment);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialogFragment confirmDialog = ConfirmDialogFragment.newInstance(
                        new DeleteConsumptionTask(getActivity().getApplicationContext(), consumption),
                        null,
                        getResources().getString(R.string.dialog_confirm_delete_consumption)
                );
                confirmDialog.show(getFragmentManager(), getString(R.string.dialog_confirm));
            }
        });

        return view;
    }

    public void setConsumption(Consumption consumption) {
        this.consumption = consumption;
    }

    @Subscribe
    public void redirectToList(DeleteConsumptionEvent event) {
        Log.d("Event", "Delete Consumption Event fetched");

        redirectFragment(new ListConsumptionFragment());
    }

    @Subscribe
    public void showConsumptionFieldsAndCategory(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        Categories categories = event.getCategories();

        TextView tvCategory = new TextView(activity);
        TextView tvName = new TextView(activity);
        TextView tvValue = new TextView(activity);
        TextView tvCreatedAt = new TextView(activity);

        String categoryName = "";
        String categoryUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == consumption.getCategoryId()) {
                    categoryName = category.getName();
                    categoryUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        tvCategory.setText(activity.getResources().getString(R.string.form_label_category) + ": " + categoryName);
        tvName.setText(activity.getResources().getString(R.string.form_label_name) + ": " + consumption.getName());
        tvValue.setText(activity.getResources().getString(R.string.form_label_value) + ": " + new DecimalFormat("#.#").format(consumption.getValue()) + categoryUnit);
        tvCreatedAt.setText(activity.getResources().getString(R.string.form_label_date) + ": " + new DateTime(consumption.getCreatedAt()).toString("yyyy-MM-dd HH:mm"));

        tvCategory.setLayoutParams(layoutParams);
        tvName.setLayoutParams(layoutParams);
        tvValue.setLayoutParams(layoutParams);
        tvCreatedAt.setLayoutParams(layoutParams);

        layoutContent.addView(tvCategory);
        layoutContent.addView(tvName);
        layoutContent.addView(tvValue);
        layoutContent.addView(tvCreatedAt);

        progressBar.setVisibility(View.GONE);
        tvHeader.setVisibility(View.VISIBLE);
        layoutContent.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        super.handleError(event);
    }
}
