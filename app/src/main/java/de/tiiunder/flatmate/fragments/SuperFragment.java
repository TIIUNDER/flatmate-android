package de.tiiunder.flatmate.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import de.tiiunder.flatmate.MainActivity;
import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.rest.SuperAsyncTask;
import de.tiiunder.flatmate.service.EventService;

public class SuperFragment extends Fragment {
    protected EventService eventService;
    protected FragmentManager fragmentManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentManager = getFragmentManager();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        eventService = EventService.getInstance();
        eventService.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        eventService.unregister(this);
    }

    public void redirectFragment(Fragment fragment) {
        closeKeyboard();

        String packageName = fragment.getClass().getPackage().getName();
        try {
            MainActivity activity = (MainActivity) getActivity();
            activity.changeFragment(fragment, true);

            if(packageName.contains("fragments.category")) {
                activity.setTitle(getResources().getString(R.string.title_categories));
                return;
            }

            if(packageName.contains("fragments.consumption")) {
                activity.setTitle(getResources().getString(R.string.title_consumptions));
                return;
            }

            if(packageName.contains("fragments.expense")) {
                activity.setTitle(getResources().getString(R.string.title_expense));
                return;
            }

            if(packageName.contains("fragments.forecast")) {
                activity.setTitle(getResources().getString(R.string.title_forecasts));
                return;
            }
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    protected void closeKeyboard() {
        Activity activity = getActivity();
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static class ConfirmDialogFragment extends android.support.v4.app.DialogFragment {
        private static ConfirmDialogFragment dialog;
        private static SuperAsyncTask task;
        private static Fragment fragmentRedirect;
        private static String message;

        public static ConfirmDialogFragment newInstance(SuperAsyncTask asyncTask, Fragment fragmentType, String dialogMessage) {
            dialog = new ConfirmDialogFragment();

            task = asyncTask;
            fragmentRedirect = fragmentType;
            message = dialogMessage;

            return dialog;
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new ProgressDialog.Builder(getActivity()).
                    setMessage(message).
                    setPositiveButton(R.string.dialog_yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if(task != null) {
                                        task.execute();
                                    }
                                    SuperFragment fragment = (SuperFragment) getFragmentManager().findFragmentById(R.id.container);
                                    if (fragmentRedirect != null) {
                                        fragment.redirectFragment(fragmentRedirect);
                                    }
                                }
                            }).
                    setNegativeButton(R.string.dialog_no,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialog.dismiss();
                                }
                            }).
                    create();
        }
    }

}
