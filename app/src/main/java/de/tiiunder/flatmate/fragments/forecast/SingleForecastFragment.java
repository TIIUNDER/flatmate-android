package de.tiiunder.flatmate.fragments.forecast;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.joda.time.DateTime;

import java.text.DecimalFormat;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Categories;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.forecast.Forecast;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.fragments.SingleFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;

public class SingleForecastFragment extends SingleFragment {
    private Forecast forecast;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Forecast single fragment started");

        if(forecast != null) {
            tvHeader.setText(getActivity().getString(R.string.title_forecast));

            new GetCategoriesTask(activity.getApplicationContext()).execute();
        }

        btnEdit.setVisibility(View.GONE);
        btnDelete.setVisibility(View.GONE);

        return view;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

    @Subscribe
    public void showForecastFieldsAndCategory(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        Categories categories = event.getCategories();

        TextView tvCategory = new TextView(activity);
        TextView tvExpense = new TextView(activity);
        TextView tvExpectedConsumption = new TextView(activity);
        TextView tvConsumptionUntilNow = new TextView(activity);
        TextView tvExpectedCosts = new TextView(activity);
        TextView tvCostsUntilNow = new TextView(activity);

        String categoryName = "";
        String categoryUnit = "";
        if(categories != null) {
            for(Category category : categories.getCategories()) {
                if(category.getId() == forecast.getCategoryId()) {
                    categoryName = category.getName();
                    categoryUnit = " " + category.getUnit();
                    break;
                }
            }
        }

        tvCategory.setText(activity.getResources().getString(R.string.form_label_category) + ": " + categoryName);
        tvExpense.setText(activity.getResources().getString(R.string.form_label_name) + ": " + forecast.getName());
        tvExpectedConsumption.setText(activity.getResources().getString(R.string.form_label_expected_consumption) + ": " + new DecimalFormat("#.#").format(forecast.getExpectedConsumption()) + categoryUnit + " " + activity.getResources().getString(R.string.string_until) + " " + new DateTime(forecast.getEndDate()).toString("yyyy-MM-dd"));
        tvConsumptionUntilNow.setText(String.format(activity.getResources().getString(R.string.form_label_consumption_until_now) + ": " + new DecimalFormat("#.#").format(forecast.getConsumptionUntilNow())) + categoryUnit + " " + activity.getResources().getString(R.string.string_since) + " " + new DateTime(forecast.getStartDate()).toString("yyyy-MM-dd"));
        tvExpectedCosts.setText(activity.getResources().getString(R.string.form_label_expected_costs) + ": " + new DecimalFormat("#.##").format(forecast.getExpectedCosts()) + "€ " + activity.getResources().getString(R.string.string_until) + " " + new DateTime(forecast.getEndDate()).toString("yyyy-MM-dd"));
        tvCostsUntilNow.setText(activity.getResources().getString(R.string.form_label_costs_until_now) + ": " + new DecimalFormat("#.##").format(forecast.getCostsUntilNow()) + "€ " + activity.getResources().getString(R.string.string_since) + " " + new DateTime(forecast.getStartDate()).toString("yyyy-MM-dd"));

        tvCategory.setLayoutParams(layoutParams);
        tvExpense.setLayoutParams(layoutParams);
        tvExpectedConsumption.setLayoutParams(layoutParams);
        tvConsumptionUntilNow.setLayoutParams(layoutParams);
        tvExpectedCosts.setLayoutParams(layoutParams);
        tvCostsUntilNow.setLayoutParams(layoutParams);

        layoutContent.addView(tvCategory);
        layoutContent.addView(tvExpense);
        layoutContent.addView(tvExpectedConsumption);
        layoutContent.addView(tvConsumptionUntilNow);
        layoutContent.addView(tvExpectedCosts);
        layoutContent.addView(tvCostsUntilNow);

        progressBar.setVisibility(View.GONE);
        tvHeader.setVisibility(View.VISIBLE);
        layoutContent.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        super.handleError(event);
    }
}
