package de.tiiunder.flatmate.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.events.ErrorEvent;

public class ListFragment extends SuperFragment {
    protected View view;
    protected ListView listView;
    protected ProgressBar progressBar;
    protected TextView tvEmpty;
    protected FloatingActionButton btnAdd;
    protected boolean categoriesLoaded;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view  = inflater.inflate(R.layout.fragment_list, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        tvEmpty = (TextView) view.findViewById(R.id.tvEmpty);
        btnAdd = (FloatingActionButton) view.findViewById(R.id.btnAdd);

        categoriesLoaded = false;

        return view;
    }

    public void handleError(ErrorEvent event) {
        progressBar.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

}
