package de.tiiunder.flatmate.fragments.consumption;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.fragments.FormFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;

public class FormConsumptionFragment extends FormFragment {
    protected EditText etName;
    protected EditText etValue;
    protected Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.form_consumption, container, false);

        etName = (EditText) view.findViewById(R.id.etName);
        etValue = (EditText) view.findViewById(R.id.etValue);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinnerCategory);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        new GetCategoriesTask(getActivity().getApplicationContext()).execute();

        return view;
    }

    protected Consumption handleFormData() {
        Category category = (Category) spinnerCategory.getSelectedItem();

        int categoryId = category.getId();
        String name = etName.getText().toString();
        double value = Double.parseDouble(etValue.getText().toString());

        Consumption consumption = new Consumption();
        consumption.setCategory(categoryId);
        consumption.setName(name);
        consumption.setValue(value);

        return consumption;
    }

    protected boolean isValid() {

        Category category = (Category) spinnerCategory.getSelectedItem();
        String name = etName.getText().toString();
        String value = etValue.getText().toString();

        boolean valid = true;

        if(category.getId() == 0 || category == null || !(category instanceof Category)) {
            View selectedView = spinnerCategory.getSelectedView();
            if(selectedView != null && selectedView instanceof TextView) {
                ((TextView) selectedView).setError(getResources().getString(R.string.valiadtion_no_category_selected));
            }
            valid = false;
        }

        if(name.length() <= 0) {
            etName.setError(getActivity().getResources().getString(R.string.validation_empty_name));
            valid = false;
        }

        if(value.length() <= 0) {
            etValue.setError(getActivity().getResources().getString(R.string.validation_empty_value));
            valid = false;
        }

        if(!value.matches("\\d+(\\.\\d+)?")) {
            etValue.setError(getActivity().getResources().getString(R.string.validation_not_number_value));
            valid = false;
        }

        return valid;
    }
}
