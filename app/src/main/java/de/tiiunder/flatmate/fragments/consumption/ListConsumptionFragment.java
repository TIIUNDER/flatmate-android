package de.tiiunder.flatmate.fragments.consumption;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.adapters.consumption.ListConsumptionAdapter;
import de.tiiunder.flatmate.entities.consumption.Consumption;
import de.tiiunder.flatmate.events.ErrorEvent;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.consumption.GetConsumptionsEvent;
import de.tiiunder.flatmate.fragments.ListFragment;
import de.tiiunder.flatmate.rest.consumption.GetConsumptionsTask;

public class ListConsumptionFragment extends ListFragment {
    private boolean consumptionsLoaded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.d("NAV", "Consumptions list fragment started");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectFragment(new AddConsumptionFragment());
            }
        });

        new GetConsumptionsTask(getActivity().getApplicationContext()).execute();

        return view;
    }

    @Subscribe
    public void fillConsumptionsList(GetConsumptionsEvent event) {
        Log.d("Event", "Get Consumptions Event fetched");

        // hide loading animation if consumptions AND categories are ready
        consumptionsLoaded = true;
        if(categoriesLoaded) {
            progressBar.setVisibility(View.GONE);
        }

        ArrayList<Consumption> consumptions = event.getConsumptions().getConsumptions();

        if (consumptions.isEmpty()) {
            tvEmpty.setText(getResources().getString(R.string.message_list_empty_consumption));
            tvEmpty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        } else {
            ListConsumptionAdapter<Consumption> adapter = new ListConsumptionAdapter<>(getActivity(), R.layout.list_item, consumptions, true);

            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Consumption consumption = (Consumption) listView.getItemAtPosition(position);

                    SingleConsumptionFragment fragment = new SingleConsumptionFragment();
                    fragment.setConsumption(consumption);

                    redirectFragment(fragment);
                }
            });        }
    }

    @Subscribe
    public void categoriesLoaded(GetCategoriesEvent event) {
        Log.d("Event", "Get Categories Event fetched");

        // hide loading animation if consumptions AND categories are ready
        categoriesLoaded = true;
        if(consumptionsLoaded) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void handleError(ErrorEvent event) {
        tvEmpty.setText(getResources().getString(R.string.message_list_empty_consumption));
        super.handleError(event);
    }
}
