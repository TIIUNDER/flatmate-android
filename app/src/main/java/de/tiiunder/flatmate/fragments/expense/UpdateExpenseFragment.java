package de.tiiunder.flatmate.fragments.expense;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.events.category.GetCategoriesEvent;
import de.tiiunder.flatmate.events.expense.UpdateExpenseEvent;
import de.tiiunder.flatmate.fragments.consumption.ListConsumptionFragment;
import de.tiiunder.flatmate.rest.expense.UpdateExpenseTask;

public class UpdateExpenseFragment extends FormExpenseFragment {
    private Expense expense;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if(expense != null) {
            etName.setText(expense.getName().toString());
            etDate.setText(expense.getDate().getYear() + "-" + expense.getDate().getMonth() + "-" + expense.getDate().getDay());
            etPeriodCount.setText(String.valueOf(expense.getPeriodCount()));
            etCount.setText(String.valueOf(expense.getCount()));
            etConsumption.setText(String.valueOf(expense.getConsumption()));
            etDeposit.setText(String.valueOf(expense.getDeposit()));
            etFee.setText(String.valueOf(expense.getFee()));
            etPeriodCountFee.setText(String.valueOf(expense.getFeePeriodCount()));
            etCostPerUnit.setText(String.valueOf(expense.getCostPerUnit()));
            fillPeriodSpinners();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    Expense handleExpense = handleFormData();
                    handleExpense.setId(expense.getId());

                    new UpdateExpenseTask(getActivity().getApplicationContext(), handleExpense).execute();
                }
            }
        });

        return view;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    @Subscribe
    public void fillCategoriesSpinner(GetCategoriesEvent event) {
        super.fillCategoriesSpinner(event);

        if(expense != null) {
            int categoryCount = 0;
            // search and select correct category
            for (Category category : event.getCategories().getCategories()) {
                if (category.getId() == expense.getCategoryId()) {
                    spinnerCategory.setSelection(categoryCount);
                    break;
                }
                categoryCount++;
            }
        }
    }

    private void fillPeriodSpinners() {
        if(expense != null) {
            spinnerPeriodBilling.setSelection(expense.getPeriodType()-1);
            spinnerPeriodFee.setSelection(expense.getFeePeriodType()-1);
        }
    }

    @Subscribe
    public void redirectToList(UpdateExpenseEvent event) {
        Log.d("Event", "Update Expense Event fetched");

        redirectFragment(new ListExpenseFragment());
    }
}
