package de.tiiunder.flatmate.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.events.ErrorEvent;

public class SingleFragment extends SuperFragment {
    protected View view;
    protected TextView tvHeader;
    protected ProgressBar progressBar;
    protected LinearLayout layoutContent;
    protected LinearLayout.LayoutParams layoutParams;
    protected FloatingActionButton btnEdit;
    protected FloatingActionButton btnDelete;
    protected Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_single, container, false);

        activity = getActivity();

        layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 10, 10, 0);

        tvHeader = (TextView) view.findViewById(R.id.tvHeader);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        layoutContent = (LinearLayout) view.findViewById(R.id.layoutContent);
        btnEdit = (FloatingActionButton) view.findViewById(R.id.btnEdit);
        btnDelete = (FloatingActionButton) view.findViewById(R.id.btnDelete);

        return view;
    }

    public void handleError(ErrorEvent event) {
        progressBar.setVisibility(View.GONE);
    }
}
