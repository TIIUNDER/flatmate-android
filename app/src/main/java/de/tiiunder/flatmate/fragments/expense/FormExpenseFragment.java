package de.tiiunder.flatmate.fragments.expense;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.joda.time.Period;

import java.util.ArrayList;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.entities.expense.Expense;
import de.tiiunder.flatmate.fragments.FormFragment;
import de.tiiunder.flatmate.rest.category.GetCategoriesTask;
import de.tiiunder.flatmate.types.DateType;
import de.tiiunder.flatmate.types.PeriodType;

public class FormExpenseFragment extends FormFragment {
    protected Spinner spinnerPeriodBilling;
    protected Spinner spinnerPeriodFee;
    protected EditText etName;
    protected EditText etDate;
    protected EditText etPeriodCount;
    protected EditText etCount;
    protected EditText etConsumption;
    protected EditText etDeposit;
    protected EditText etFee;
    protected EditText etPeriodCountFee;
    protected EditText etCostPerUnit;
    protected Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.form_expense, container, false);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinnerCategory);

        new GetCategoriesTask(getActivity().getApplicationContext()).execute();

        spinnerPeriodBilling = (Spinner) view.findViewById(R.id.spinnerPeriodBilling);
        spinnerPeriodFee = (Spinner) view.findViewById(R.id.spinnerPeriodFee);
        fillPeriodsSpinner();

        etName = (EditText) view.findViewById(R.id.etName);
        etDate = (EditText) view.findViewById(R.id.etDate);
        etPeriodCount = (EditText) view.findViewById(R.id.etPeriodCount);
        etCount = (EditText) view.findViewById(R.id.etCount);
        etConsumption = (EditText) view.findViewById(R.id.etConsumption);
        etDeposit = (EditText) view.findViewById(R.id.etDeposit);
        etFee = (EditText) view.findViewById(R.id.etFee);
        etPeriodCountFee = (EditText) view.findViewById(R.id.etFeePeriodCount);
        etCostPerUnit = (EditText) view.findViewById(R.id.etCostPerUnit);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        setupDatePicker(etDate);

        return view;
    }

    protected Expense handleFormData() {
        Expense expense = new Expense();

        Category category = (Category) spinnerCategory.getSelectedItem();

        int categoryId = category.getId();
        expense.setCategory(categoryId);

        String name = etName.getText().toString();
        expense.setName(name);

        DateType date = new DateType(etDate.getText().toString());
        expense.setDate(date);

        try {
            int periodCount = Integer.parseInt(etPeriodCount.getText().toString());
            expense.setPeriodCount(periodCount);
        } catch (NumberFormatException ex) {

        }

        int periodTypeValue = ((PeriodType) spinnerPeriodBilling.getSelectedItem()).getValue();
        expense.setPeriodType(periodTypeValue);

        try {
            double count = Double.parseDouble(etCount.getText().toString());
            expense.setCount(count);
        } catch (NumberFormatException ex) {

        }

        try {
            double consumption = Double.parseDouble(etConsumption.getText().toString());
            expense.setConsumption(consumption);
        } catch (NumberFormatException ex) {

        }

        try {
            double deposit = Double.parseDouble(etDeposit.getText().toString());
            expense.setDeposit(deposit);
        } catch (NumberFormatException ex) {

        }

        try {
            double fee = Double.parseDouble(etFee.getText().toString());
            expense.setFee(fee);
        } catch (NumberFormatException ex) {

        }

        try {
            int periodCountFee = Integer.parseInt(etPeriodCountFee.getText().toString());
            expense.setFeePeriodCount(periodCountFee);
        } catch (NumberFormatException ex) {

        }

        int periodTypeFeeValue = ((PeriodType) spinnerPeriodFee.getSelectedItem()).getValue();
        expense.setFeePeriodType(periodTypeFeeValue);

        try {
            double costPerUnit = Double.parseDouble(etCostPerUnit.getText().toString());
            expense.setCostPerUnit(costPerUnit);
        } catch (NumberFormatException ex) {

        }

        return expense;
    }

    private void fillPeriodsSpinner() {
        ArrayList<PeriodType> periodTypes = new ArrayList<>();
        periodTypes.add(new PeriodType(PeriodType.PERIOD_TYPE_DAY, getResources().getString(R.string.form_period_days)));
        periodTypes.add(new PeriodType(PeriodType.PERIOD_TYPE_WEEK, getResources().getString(R.string.form_period_weeks)));
        periodTypes.add(new PeriodType(PeriodType.PERIOD_TYPE_MONTH, getResources().getString(R.string.form_period_months)));
        periodTypes.add(new PeriodType(PeriodType.PERIOD_TYPE_YEAR, getResources().getString(R.string.form_period_years)));

        ArrayAdapter<PeriodType> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, periodTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (spinnerPeriodBilling != null) {
            spinnerPeriodBilling.setAdapter(adapter);
        }

        if (spinnerPeriodFee != null) {
            spinnerPeriodFee.setAdapter(adapter);
        }
    }

    protected boolean isValid() {

        Category category = (Category) spinnerCategory.getSelectedItem();
        String name = etName.getText().toString();
        String date = etDate.getText().toString();
        String periodCount = etPeriodCount.getText().toString();
        PeriodType periodType = (PeriodType) spinnerPeriodBilling.getSelectedItem();
        String count = etCount.getText().toString();
        String consumption = etConsumption.getText().toString();
        String deposit = etDeposit.getText().toString();
        String fee = etFee.getText().toString();
        String feePeriodCount = etPeriodCountFee.getText().toString();
        PeriodType feePeriodType = (PeriodType) spinnerPeriodFee.getSelectedItem();
        String costPerUnit = etCostPerUnit.getText().toString();

        // TODO use validation groups like in web app

        boolean valid = true;

        if(category.getId() == 0 || category == null || !(category instanceof Category)) {
            View selectedView = spinnerCategory.getSelectedView();
            if(selectedView != null && selectedView instanceof TextView) {
                ((TextView) selectedView).setError(getResources().getString(R.string.valiadtion_no_category_selected));
            }
            valid = false;
        }

        if (name.length() <= 0) {
            etName.setError(getResources().getString(R.string.validation_empty_name));
            valid = false;
        }

        if (date.length() <= 0) {
            etDate.setError(getResources().getString(R.string.validation_empty_date));
            valid = false;
        } else {
            etDate.setError(null); // workaround for not-focussable date field
        }

        // TODO check date format

        if (periodType == null) {
            // TODO show error
            valid = false;
        }

        if (!periodCount.matches("\\d+")) {
            etPeriodCount.setError(getResources().getString(R.string.validation_not_number_period));
            valid = false;
        }

        if (periodCount.length() <= 0) {
            etPeriodCount.setError(getResources().getString(R.string.validation_empty_period));
            valid = false;
        }

        if (!count.matches("\\d+(\\.\\d+)?")) {
            etCount.setError(getResources().getString(R.string.validation_not_number_count));
            valid = false;
        }

        if (count.length() <= 0) {
            etCount.setError(getResources().getString(R.string.validation_empty_count));
            valid = false;
        }

        if (!consumption.matches("\\d+(\\.\\d+)?")) {
            etConsumption.setError(getResources().getString(R.string.validation_not_number_consumption));
            valid = false;
        }

        if (consumption.length() <= 0) {
            etConsumption.setError(getResources().getString(R.string.validation_empty_consumption));
            valid = false;
        }

        if (!deposit.matches("\\d+(\\.\\d+)?")) {
            etDeposit.setError(getResources().getString(R.string.validation_not_number_deposit));
            valid = false;
        }

        if (deposit.length() <= 0) {
            etDeposit.setError(getResources().getString(R.string.validation_empty_deposit));
            valid = false;
        }

        if (feePeriodType == null) {
            // TODO show error
            valid = false;
        }

        if (!fee.matches("\\d+(\\.\\d+)?")) {
            etFee.setError(getResources().getString(R.string.validation_not_number_fee));
            valid = false;
        }

        if (fee.length() <= 0) {
            etFee.setError(getResources().getString(R.string.validation_empty_fee));
            valid = false;
        }

        if (!feePeriodCount.matches("\\d+")) {
            etPeriodCountFee.setError(getResources().getString(R.string.validation_not_number_fee_period));
            valid = false;
        }

        if (feePeriodCount.length() <= 0) {
            etPeriodCountFee.setError(getResources().getString(R.string.validation_empty_fee_period));
            valid = false;
        }

        if (!costPerUnit.matches("\\d+(\\.\\d+)?")) {
            etCostPerUnit.setError(getResources().getString(R.string.validation_not_number_cost_per_unit));
            valid = false;
        }

        if (costPerUnit.length() <= 0) {
            etCostPerUnit.setError(getResources().getString(R.string.validation_empty_cost_per_unit));
            valid = false;
        }

        return valid;
    }
}
