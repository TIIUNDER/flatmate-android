package de.tiiunder.flatmate.fragments.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import de.tiiunder.flatmate.R;
import de.tiiunder.flatmate.entities.category.Category;
import de.tiiunder.flatmate.fragments.FormFragment;

public class FormCategoryFragment extends FormFragment {
    protected EditText etName;
    protected EditText etUnit;
    protected Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view  = inflater.inflate(R.layout.form_category, container, false);

        etName = (EditText) view.findViewById(R.id.etName);
        etUnit = (EditText) view.findViewById(R.id.etUnit);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        return view;
    }

    protected Category handleFormData() {
        String name = etName.getText().toString();
        String unit = etUnit.getText().toString();

        Category category = new Category();
        category.setName(name);
        category.setUnit(unit);

        return category;
    }

    protected boolean isValid() {

        String name = etName.getText().toString();
        String unit = etUnit.getText().toString();

        boolean valid = true;

        if(name.length() <= 0) {
            etName.setError(getResources().getString(R.string.validation_empty_name));
            valid = false;
        }

        if(unit.length() <= 0) {
            etUnit.setError(getResources().getString(R.string.validation_empty_unit));
            valid = false;
        }

        return valid;

    }
}
