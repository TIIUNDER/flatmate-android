package de.tiiunder.flatmate.types;

import android.content.Context;

import de.tiiunder.flatmate.R;

public class PeriodType {
    public static final int PERIOD_TYPE_DAY = 1;
    public static final int PERIOD_TYPE_WEEK = 2;
    public static final int PERIOD_TYPE_MONTH = 3;
    public static final int PERIOD_TYPE_YEAR = 4;

    private int value;
    private String name;

    public PeriodType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String periodName(Context context, int periodType) {
        String result;

        // TODO handle singular and plural
        switch (periodType) {
            case PERIOD_TYPE_DAY:
                result = context.getResources().getString(R.string.form_period_days);
                break;
            case PERIOD_TYPE_WEEK:
                result = context.getResources().getString(R.string.form_period_weeks);
                break;
            case PERIOD_TYPE_MONTH:
                result = context.getResources().getString(R.string.form_period_months);
                break;
            case PERIOD_TYPE_YEAR:
                result = context.getResources().getString(R.string.form_period_years);
                break;
            default:
                result = "";
                break;
        }

        return result;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
