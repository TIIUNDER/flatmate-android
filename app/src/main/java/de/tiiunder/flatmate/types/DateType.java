package de.tiiunder.flatmate.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@JsonRootName("date")
public class DateType {
    @JsonProperty
    private int day;

    @JsonProperty
    private int month;

    @JsonProperty
    private int year;

    public DateType() {

    }

    public DateType(String datetime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Calendar calendar = Calendar.getInstance();
            Date date = format.parse(datetime);
            calendar.setTime(date);

            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Date{" +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
