# Flatmate

Android App

## Links

* [Web-App](https://github.com/TIIUNDER/flatmate/)

## Ressources

* [Android Asset Studio Icon Generator](https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html)

    * The generated Icons are licensed under [Creative Commons 3.0](http://creativecommons.org/licenses/by/3.0/)

## License

[MIT License](http://opensource.org/licenses/MIT)

See [LICENSE](https://github.com/TIIUNDER/flatmate-android/blob/master/LICENSE) for more information